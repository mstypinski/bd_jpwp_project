-- create DB

create table users (
	user_id serial primary key,
	name varchar(20),
	surname varchar(20),
	email varchar(30),
	login varchar(20),
	password varchar(32)
);

create table driver_journeys (
	driver_journey_id serial primary key,
	driver_id int4, 
	seats int2 not null,
	journey_date timestamp not null,
	minutes_offset int2 not null,
	start_point circle not null,
	end_point circle not null,
	price float not null,
	foreign key (driver_id) references users (user_id)
);
/*
create table passenger_journeys (
	passenger_journey_id serial primary key,
	passenger_id int4 not null,
	seats_needed int2 not null,
	journey_date timestamp not null,
	minutes_offset int2 not null,
	start_point circle not null,
	end_point circle not null,
	foreign key (passenger_id) references users (user_id)
);
*/
create table driver_passenger_mapping (
	driver_journey_id int4,
	foreign key (driver_journey_id) references driver_journeys (driver_journey_id)
	seats int2 not null,
	foreign key (driver_journey_id) references driver_journeys (driver_journey_id)
);
